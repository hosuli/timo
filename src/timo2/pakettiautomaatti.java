
package timo2;


public class pakettiautomaatti {
    private String name;
    private String opentimes;
    private String address;
    private String city;
    private String code;
    private String latitude;
    private String lng;
    
    public pakettiautomaatti(String n, String o, String a, String c, String co, String la, String lo){
        name = n;
        opentimes = o;
        address = a;
        city = c;
        code = co;
        latitude = la;
        lng = lo;
    }
    public String getName() {
        return name;
    }
    public String getOpenTimes(){
        return opentimes;
    }
    public String getCity() {
        return city;
    }
    public String getAddress() {
        return address;
    }
    public String getLatitude() {
        return latitude;
    }
    public String getLng() {
        return lng;
    }
    public String getCode() {
        return code;
    }
    public String toString(){
        String c = city;
        return c;
    }
}
