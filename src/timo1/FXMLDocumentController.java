package timo1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.web.WebView;
import javax.swing.JOptionPane;
import timo2.PStorage;
import timo2.packet;
import timo2.pakettiautomaatti;
import timo2.parsinta;
import timo1.LokiController;

public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TabPane tabPane;
    @FXML
    private WebView web;
    private ArrayList<pakettiautomaatti> automatlist;
    @FXML
    private Button createPacket;
    @FXML
    private ComboBox<String> cityCombo;
    @FXML
    private Button addToMapButton;
    @FXML
    private Button updateButton;
    @FXML
    private Button sendButton;
    @FXML
    private Button deleteRoutesButton;
    private int citycomboindex;
    @FXML
    private ComboBox<String> createdPackages;
    private ArrayList<packet> packetlist;
    private ArrayList<Float> long_lat_list;
    private int doesBreak;
    @FXML
    private Tab mapTab;
    @FXML
    private Tab packetTab;
    @FXML
    private Tab logTab;

    @FXML
    private void handleButtonAction(ActionEvent event) {    /*luo paketti button opens the create packet tab*/
        tabPane.getSelectionModel().selectNext();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb){
        Image mapImage = new Image("map35(2).png");
        ImageView iv = new ImageView(mapImage);
        mapTab.setGraphic(iv);
//        Image image = new Image("map35(2).png"); /*Icon made by simpleicon from www.flaticon.com*/
//        ImageView iv = new ImageView(image);
//        mapTab.setGraphic(iv);
        Image boxImage = new Image("boxes6.png"); /*Icon made by Freepik from www.flaticon.com*/
        ImageView iv2 = new ImageView(boxImage);
        packetTab.setGraphic(iv2);
        Image logImage = new Image("contract11.png"); /*Icon made by Freepik from www.flaticon.com*/
        ImageView iv3 = new ImageView(logImage);
        logTab.setGraphic(iv3);
        
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());    /*opens index.html in webview*/
        try {
            callParsing();
            FXMLWebViewController lol = new FXMLWebViewController();
            lol.bringList(automatlist); /*brings automatlist from FXMLWebViewController*/
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        createdPackages.getItems().clear();   /*default items away from the combobox*/
    }    

    
    private void callParsing() throws MalformedURLException, IOException{   /*calls parsing*/
        URL url1 = new URL("http://smartpost.ee/fi_apt.xml");
        BufferedReader br = new BufferedReader(new InputStreamReader(url1.openStream()));
        String content = "";
        String line;
        while ((line = br.readLine()) != null) {    /*saves contents of xml file to content*/
            content += line + "\n";
        }
        parsinta p = new parsinta(content);
        automatlist = p.getArray();
        cityCombo.getItems().clear();   /*clears cityCombo of stupid default items*/
        for (pakettiautomaatti pa : automatlist){   /*adding cities to cityCombo*/
            if(cityCombo.getItems().contains(pa.getCity())){    /*if city has already been added: do nothing*/    
            }else {
                cityCombo.getItems().add(pa.getCity()); /*if city has not been added: add*/
            }
        }
    }
    
    public static void infoBox(String infoMessage, String titleBar){    /*shows a nice little pop up window ('cause we all loooove pop ups)*/
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + titleBar, JOptionPane.INFORMATION_MESSAGE);
    }


    @FXML
    private void addToMapAction(ActionEvent event) {    /*adding all smarpost automats in the city (chosen in citycombo) to a map*/
        String temp2 = cityCombo.getValue();
        for (pakettiautomaatti pa : automatlist){
            if (pa.getCity().equals(temp2)){
                String addresstemp = pa.getAddress() + pa.getCity();
                web.getEngine().executeScript("document.goToLocation(\""+addresstemp+"\", \""+pa.getOpenTimes()+ "\" , 'blue')"); /*shows blue thingy on location and shows opening hours*/
            }
        }
    }

    @FXML
    private void updateAction(ActionEvent event) {  /*adds created packets to the addpackagecombo*/
        this.addPackageCombo();
    }

    @FXML
    private void sendAction(ActionEvent event) {    /*sending the packet*/ 
        LokiController lc = LokiController.getInstance();
        long_lat_list = new ArrayList<>(); /*new list*/
        String totemp;
        String fromtemp;
        boolean Breakability;
        int Pclass;
        int counter = -1;
        String logtemp1 = "";
        String logtemp2 = "";
        String logtemp3 = "";
        String logtemp4 = "";
        String temp3 = createdPackages.getValue();
        createdPackages.valueProperty().set(null);
        for (packet pack : packetlist){
            counter = counter + 1;
            if (pack.getName().equals(temp3)) {
                totemp = pack.getToName();
                fromtemp = pack.getFromName();
                for (pakettiautomaatti pa: automatlist){
                    if (pa.getName().equals(fromtemp)){
                        float startlat = Float.parseFloat(pa.getLatitude());
                        float startlng = Float.parseFloat(pa.getLng());
                        long_lat_list.add(startlat);    /*adds coordinates to the list*/
                        long_lat_list.add(startlng);
                        logtemp1 = pa.getCity();
                        logtemp2 = pa.getName();
                    }
                }
                for (pakettiautomaatti pa2: automatlist){
                    if (pa2.getName().equals(totemp)){
                        float endlat = Float.parseFloat(pa2.getLatitude());
                        float endlng = Float.parseFloat(pa2.getLng());
                        long_lat_list.add(endlat);  /*adds coordinates to the list*/
                        long_lat_list.add(endlng);
                        logtemp3 = pa2.getCity();
                        logtemp4 = pa2.getName();
                    }
                }
                createdPackages.getItems().remove(temp3);
                double length = (double)web.getEngine().executeScript("document.createPath("+long_lat_list+", 'red' , "+(int)pack.getPClass()+" )");    /*draws a path from starting point to ending point*/
                doesBreak = checkIfBreaks(pack.getBreakability(), pack.getPClass());    /*checks if the item get broken while being transported*/
                
                if (doesBreak == 1) {   /*when packet get broken*/
                    infoBox("Ikävä kyllä toimituksesi ei selvinnyt ehjänä kuljetuksesta", "");  /*yipeee it's a pop up window <33*/
                }
                lc.packetSent(pack.getName(), logtemp1 , logtemp2, logtemp3, logtemp4, length, doesBreak);
                PStorage.removeItem(counter);
                break;
            }
        }
    }

    @FXML
    private void deleteRoutes(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");    /*deletes all paths from the map*/
    }
    
    public ArrayList getArray(){
        return automatlist;
    }
    
    public int checkIfBreaks(boolean br, int PC) { /*Returns 1 if breaks, 0 if not*/
        if (PC == 2){   /*2nd class package:*/
            return 0;   /*never breaks*/
        } else if (PC == 1 && br == true){  /*1st class package, item can break*/
            Random rn = new Random();   /*uses random number between 0 and 10 to determine whether breaks or not*/
            int range = 10 - 0 + 1;
            int randomNum =  rn.nextInt(range) + 0;
            if (randomNum <= 6) {   /*when numer is smaller than 7*/
                System.out.println("Hups hajos");   /*it breaks*/
                return 1;
            } else {    /*otherwise doesn't break*/
                System.out.println("Ehjäksi jäi (:");
                return 0;
            }
        } else if (PC == 1 && br == false){ /*1st class package, item can't get broken*/
            return 0;
        } else if (PC == 3 && br == true){ /*3rd class package, item can get broken*/
            return 1;   /*gets always broken*/
        } else if (PC == 3 && br == false){ /*3rd class package, item can't get broken*/
            return 0;   /*obvs doesn't since it CAN'T BREAK*/ 
        }
        return 0; /*random return value that never gets returned*/
    }
    
    public void addPackageCombo (){ /*adds packages to the cretedPackages combo*/
        createdPackages.getItems().clear();
        packetlist = PStorage.getArray(); /*gets list from PStorage*/
        for (packet pack : packetlist){
            createdPackages.getItems().add(pack.getName());
        }
    }
}
