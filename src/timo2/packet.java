package timo2;

public abstract class packet {
    protected String Name;
    protected int Height;
    protected int Depth;
    protected int Width;
    protected double Weight;
    protected boolean Breakability;
    protected String toCode;
    protected String fromCode;
    protected int PClass;
    protected String toP;
    protected String fromP;
    protected double distance;
    
    public packet (String name, int h, int d, int w, double we, boolean br, String to, String from, int pc, String fromplace, String toplace, double dist){
        Name = name;
        Height = h;
        Depth = d;
        Width = w;
        Weight = we;
        Breakability = br;
        toCode = to;
        fromCode = from;
        PClass = pc;
        fromP = fromplace;
        toP = toplace;
        distance = dist;
//        DeliveryNumber = DN;
    }
    
    public String getName(){
        return Name;
    }
    public String gettoCode() {
        return toCode;
    }
    public String getfromCode() {
        return fromCode;
    }
    public int getPClass(){
        return PClass;
    }
    public String getToName(){
        return toP;
    }
    public String getFromName(){
        return fromP;
    }
    public boolean getBreakability() {
        return Breakability;
    }
    public double getDistance() {
        return distance;
    }
}
