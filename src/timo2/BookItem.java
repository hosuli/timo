package timo2;

public class BookItem {
    private String Name;
    private int Height;
    private int Depth;
    private int Width;
    private double Weight;
    private boolean Breakability;
    private int PClass;
    
    public BookItem(){
        Name = "Perkeleesti kirjoja";
        Height = 59;
        Depth = 40;
        Width = 55;
        Weight = 49;
        Breakability = false;
        PClass = 3;
    }
    
    public String getName(){
        return Name;
    }
    public int getHeight(){
        return Height;
    }
    public int getDepth(){
        return Depth;
    }
    public int getWidth(){
        return Width;
    }
    public double getWeight(){
        return Weight;
    }
    public boolean getBreak(){
        return Breakability;
    }
    public int getPClass(){
        return PClass;
    }
}
