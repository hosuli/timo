package timo2;

public class CoffeeItem {
    private String Name;
    private int Height;
    private int Depth;
    private int Width;
    private double Weight;
    private boolean Breakability;
    private int PClass;
    
    public CoffeeItem(){
        Name = "Kahvipaketti";
        Height = 17;
        Depth = 6;
        Width = 9;
        Weight = 1;
        Breakability = false;
        PClass = 1;
    }
    
    public String getName(){
        return Name;
    }
    public int getHeight(){
        return Height;
    }
    public int getDepth(){
        return Depth;
    }
    public int getWidth(){
        return Width;
    }
    public double getWeight(){
        return Weight;
    }
    public boolean getBreak(){
        return Breakability;
    }
    public int getPClass(){
        return PClass;
    }
}
