package timo2;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.InputSource;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;


public class parsinta {
    private Document doc;
    private ArrayList<pakettiautomaatti> automatlist;

    public parsinta(String content) {   /*gets content in the right form from FXMLDocumentController*/
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
//            System.out.println(content);
            parseCurrentData();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(parsinta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void parseCurrentData() {   /*parsing the data*/
        try {
            NodeList nList = doc.getElementsByTagName("place");
            automatlist = new ArrayList();  /*new ArrayList automatlist*/
             for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                    /*gets elements from given contents in a given order and puts them to the automatlist*/
                automatlist.add(new pakettiautomaatti(eElement.getElementsByTagName("postoffice").item(0).getTextContent(),
                                                        eElement.getElementsByTagName("availability").item(0).getTextContent(),
                                                        eElement.getElementsByTagName("address").item(0).getTextContent(),
                                                        eElement.getElementsByTagName("city").item(0).getTextContent(),
                                                        eElement.getElementsByTagName("code").item(0).getTextContent(),
                                                        eElement.getElementsByTagName("lat").item(0).getTextContent(),
                                                        eElement.getElementsByTagName("lng").item(0).getTextContent()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public ArrayList getArray(){
        return automatlist;
    }
}
