package timo2;

public class DogeItem {
    private String Name;
    private int Height;
    private int Depth;
    private int Width;
    private double Weight;
    private boolean Breakability;
    private int PClass;
    
     public DogeItem(){
        Name = "Doge";
        Height = 25;
        Depth = 15;
        Width = 22;
        Weight = 10;
        Breakability = true;
        PClass = 2;
    }
     
    public String getName(){
        return Name;
    }
    public int getHeight(){
        return Height;
    }
    public int getDepth(){
        return Depth;
    }
    public int getWidth(){
        return Width;
    }
    public double getWeight(){
        return Weight;
    }
    public boolean getBreak(){
        return Breakability;
    }
    public int getPClass(){
        return PClass;
    }
}
