
package timo1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.Math.round;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Font;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import timo2.PClass1;
import timo2.PClass2;
import timo2.PClass3;
import timo2.PStorage;
import timo2.pakettiautomaatti;
import timo2.parsinta;
import timo2.DogeItem;
import timo2.CoffeeItem;
import timo2.BookItem;
import timo2.AppleItem;
import timo1.LokiController;


public class FXMLWebViewController implements Initializable {
    @FXML
    private Font x1;
    @FXML
    private ComboBox<String> itemCombo;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField weightField;
    @FXML
    private CheckBox breakingStuffBox;
    @FXML
    private Button infoButton;
    @FXML
    private ComboBox<String> startCityCombo;
    @FXML
    private ComboBox<String> endCity;
    @FXML
    private ComboBox<String> endAutomat;
    @FXML
    private Button emptyButton;
    @FXML
    private Button createButton;
    private ArrayList<pakettiautomaatti> automatlist;
    @FXML
    private ComboBox<String> startautomatCombo;
    @FXML
    private RadioButton firstClass;
    @FXML
    private RadioButton secondClass;
    @FXML
    private RadioButton thirdClass;
    @FXML
    private TextField sizeField3;
    @FXML
    private TextField sizeField2;
    private PStorage storage;
    @FXML
    private WebView web2;
    private ArrayList<Float> coordinates;
    @FXML
    private Label nameLabel;
    @FXML
    private Label sizeLabel;
    @FXML
    private Label weightLabel;
    @FXML
    private Label startLabel;
    @FXML
    private Label endLabel;
    @FXML
    private Label allLabel;
    @FXML
    private Label tooBigError;
    @FXML
    private Label tooBigError2;
    @FXML
    private Label tooFarLabel;
//    private static LokiController lc;
    @FXML
    private Label sameError;

   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
        ToggleGroup group = new ToggleGroup();  /*all radio button are set to be part of the same group (only one can be selected at a time)*/
        firstClass.setToggleGroup(group);
        firstClass.setSelected(true);   /*sets 1st class button as default*/
        secondClass.setToggleGroup(group);
        thirdClass.setToggleGroup(group);
        
        
        startCityCombo.getItems().clear(); 
        startautomatCombo.getItems().clear();
        endCity.getItems().clear();    /*clears combos from annoying default items*/
        endAutomat.getItems().clear();
        itemCombo.getItems().clear();
        storage = new PStorage();   /*new storage*/
        web2.getEngine().load(getClass().getResource("index.html").toExternalForm());   /*invisible webview used to find out distances later on*/
        try {
            combosetting(); /*calls combosetting (sets right things to comboboxes)*/
            itemComboSetting();
        } catch (IOException ex) {
            Logger.getLogger(FXMLWebViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 

    @FXML
    private void itemComboAction(ActionEvent event) {
        
    }
   
    @FXML
    private void breakingStuffBoxAction(ActionEvent event) {
    }

    @FXML
    private void infoAction(ActionEvent event) {    /*"infoa luokista" button opens new window that had info about classes, duh*/
        try {
            Stage classInfo = new Stage();  /*new stage*/
            Parent page = FXMLLoader.load(getClass().getResource("classInfo.fxml"));
            Scene scene = new Scene(page);  /*new scene*/
            classInfo.setScene(scene);  /*sets classInfo.fxml to open up in a new window*/
            classInfo.show();   /*shows classInfo in the new window*/
        } catch (IOException ex) {
            Logger.getLogger(FXMLWebViewController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void startCityComboAction(ActionEvent event) {  /*after choosiing city adds smartpost automats in the chosen city to automatcombo*/
        startautomatCombo.getItems().clear();   
        String temp = startCityCombo.getValue();
        for (pakettiautomaatti pa : automatlist){
            if (pa.getCity().equals(temp)){     /*finds the names of the automats from the list*/
                startautomatCombo.getItems().add(pa.getName());
            }
        }
    }

   @FXML
    private void endCityAction(ActionEvent event) { /*same as above*/
        endAutomat.getItems().clear();
        String temp = endCity.getValue();
        for (pakettiautomaatti pa : automatlist){
            if (pa.getCity().equals(temp)){
                endAutomat.getItems().add(pa.getName());
            }
        }
    }

    @FXML
    private void endAutomatAction(ActionEvent event) {
    }

    @FXML
    private void emptyAction(ActionEvent event) { /*clears all textfield and combobox selections*/
        clearAction();                             
    }
    
//    public void doSomethingLog(LokiController lc, String n, String sc, String sa, String ec, String ea, double l) {
//        lc.packetcreated(n, sc, sa, ec, ea, l);
//    }


    @FXML
    private void createAction(ActionEvent event) {  /*creates a packer*/
//        this.lc = lc;
//        LokiController lc = new LokiController();
         LokiController lc = LokiController.getInstance();
        double distance;
        coordinates = new ArrayList<>();     /*new list coordinates*/
        String toC = "";
        String toName = endAutomat.getValue();
        hideErrorMessages();    /*hides error labels*/
        for (pakettiautomaatti pa : automatlist){   /*searches the right spot from the list*/
            if (pa.getName().equals(toName)){
//                toC = pa.getCode();
                float startlat = Float.parseFloat(pa.getLatitude());    /*and saves its latitude and longtitude as floats*/
                float startlng = Float.parseFloat(pa.getLng());
                coordinates.add(startlat);  /*adds coordinates to the list*/
                coordinates.add(startlng);
            }
        }
         String fromC = "";
         String fromlat = "";
         String fromlng = "";
         String fromName = startautomatCombo.getValue();
         for (pakettiautomaatti pa : automatlist){
            if (pa.getName().equals(fromName)){     /*finds the right spot from the list*/
//                fromC = pa.getCode();
                float endlat = Float.parseFloat(pa.getLatitude());
                float endlng = Float.parseFloat(pa.getLng());    
                coordinates.add(endlat);    /*adds coordinates to the list*/
                coordinates.add(endlng);
            }
        }
        String name = nameField.getText();
        boolean br = breakingStuffBox.isSelected();
        /*package creation and error label messages thingies */
        if ((itemCombo.getItems().indexOf(itemCombo.getValue())) == -1){    /*WHEN ITEM ISN'T CHOSEN FROM THE ITEMCOMBO*/
            if (sizeField.getText().equals("") || sizeField2.getText().equals("")|| sizeField3.getText().equals("")|| nameField.getText().equals("")||
                    weightField.getText().equals("")|| endAutomat.getItems().indexOf(endAutomat.getValue()) == -1 
                    || startautomatCombo.getItems().indexOf(startautomatCombo.getValue()) == -1){
                errorMessages();    /*tells the user which things are missing*/
            
            } else {    /*kaikki kohdat on täytetty*/
                int length = Integer.parseInt(sizeField.getText());     /*saves inputs given by the user*/
                int height = Integer.parseInt(sizeField3.getText());
                int width = Integer.parseInt(sizeField2.getText());
                double weight = Double.parseDouble(weightField.getText());
                distance = checkDistance(coordinates);  /*finds out the distance between smartpost automats*/
                if (firstClass.isSelected()){   /*1st class chosen*/
                    if (length <=  60 && width <= 40 && height <= 60 && weight <= 30 && distance <= 150){   /*when things go smooooooth*/
                        PClass1 packet = new PClass1(name, height, length, width, weight, br, toC, fromC, 1, fromName, toName, distance); /*new packet*/
                        lc.packetcreated(name, startCityCombo.getValue(), startautomatCombo.getValue(), endCity.getValue(), endAutomat.getValue(), distance);   /*adds to log*/
                        storage.addpackage(packet);  /*adds packet to storage*/
                    } else {
                        tooBigError(length, width, height, weight, 1);  /*displays a label that tells that the packet is too big*/
                        tooFarError(distance);  /*informs about a too long distance*/
                    }
                } else if (secondClass.isSelected()){   /*2nd class chosen*/
                    if (length <=  30 && width <= 20 && height <= 30 && weight <= 12){  /*when packet is of right size*/
                        PClass2 packet2 = new PClass2(name, height, length, width, weight, br, toC, fromC, 2, fromName, toName, distance);    /*new packet*/
                        lc.packetcreated(name, startCityCombo.getValue(), startautomatCombo.getValue(), endCity.getValue(), endAutomat.getValue(), distance);   /*adds to log*/
                        storage.addpackage(packet2); /*adds packet to storage*/
                    } else {
                        tooBigError(length, width, height, weight, 2);  /*displays a label that tells that the packet is too big*/
                    }
                } else { /*3rd class chosen*/
                    if (length <=  80 && width <= 60 && height <= 80 && weight <= 50){  /*when packet is of right size*/
                        PClass3 packet3 = new PClass3(name, height, length, width, weight, br, toC, fromC, 3, fromName, toName, distance);     /*new packet*/
                        lc.packetcreated(name, startCityCombo.getValue(), startautomatCombo.getValue(), endCity.getValue(), endAutomat.getValue(), distance);   /*adds to log*/
                        storage.addpackage(packet3); /*adds packet to storage*/
                    } else {
                        tooBigError(length, width, height, weight, 3);  /*displays a label that tells that the packet is too big*/
                    }
                }
            }
        } else { /*when an item is chosen from itemcombo*/
            distance = checkDistance(coordinates);  /*finds out the distance between smartpost automats*/
            if(itemCombo.getValue().equals("Doge")){    /*when Doge is chosen*/
                if(endAutomat.getItems().indexOf(endAutomat.getValue()) == -1   /*checks if cities and automats have been chosen*/
                    || startautomatCombo.getItems().indexOf(startautomatCombo.getValue()) == -1){
                    tooFarLabel.setText("Valitse kaupungit ja automaatit.");
                } else {
                    DogeItem DI = new DogeItem();
                    name = "Doge";
                    PClass2 packet2 = new PClass2(DI.getName(), DI.getHeight(),     /*new packet*/
                            DI.getDepth(), DI.getWidth(), DI.getWeight(), DI.getBreak(), toC, fromC, 2, fromName, toName, distance);
                    lc.packetcreated(name, startCityCombo.getValue(), startautomatCombo.getValue(), endCity.getValue(), endAutomat.getValue(), distance);   /*adds to log*/
                    storage.addpackage(packet2);    /*adds packet to storage*/
                }
                
            } else if (itemCombo.getValue().equals("Kahvipaketti")){ /*when Kahvipaketti is chosen*/
                if(endAutomat.getItems().indexOf(endAutomat.getValue()) == -1   /*when something ain't right*/
                    || startautomatCombo.getItems().indexOf(startautomatCombo.getValue()) == -1 || distance > 150){
                    if (distance > 150) {   /*if distance is over 150 km*/
                        tooFarError(distance); /*tells user distance is too long*/
                    }else {
                        tooFarLabel.setText("Valitse kaupungit ja automaatit.");  
                    }
                    
                } else { /*when everything's on place*/
                    CoffeeItem CI = new CoffeeItem();   /*new coffeeitem*/
                    name = "Kahvipaketti";
                    PClass1 packet = new PClass1(CI.getName(), CI.getHeight(), CI.getDepth(),    /*new packet*/
                            CI.getWidth(), CI.getWeight(), CI.getBreak(), toC, fromC, 1, fromName, toName, distance);
                    lc.packetcreated(name, startCityCombo.getValue(), startautomatCombo.getValue(), endCity.getValue(), endAutomat.getValue(), distance);   /*adds to log*/
                    storage.addpackage(packet);  /*adds packet to storage*/
                }
                
            } else if (itemCombo.getValue().equals("Perkeleesti kirjoja")){ /*when a shitload of books is chosen*/
                if(endAutomat.getItems().indexOf(endAutomat.getValue()) == -1   /*somethin'g wrong*/
                    || startautomatCombo.getItems().indexOf(startautomatCombo.getValue()) == -1){
                    tooFarLabel.setText("Valitse kaupungit ja automaatit.");    /*tells the user what to do*/
                } else {    /*when cities and automats are chosen*/
                    BookItem BI = new BookItem();
                    name = "Perkeleesti kirjoja";
                    PClass3 packet = new PClass3(BI.getName(), BI.getHeight(), BI.getDepth(),
                            BI.getWidth(), BI.getWeight(), BI.getBreak(), toC, fromC, 3, fromName, toName, distance);  /*new packet*/
                    lc.packetcreated(name, startCityCombo.getValue(), startautomatCombo.getValue(), endCity.getValue(), endAutomat.getValue(), distance);   /*adds to log*/
                    storage.addpackage(packet); /*adds packet to storage*/
                }
            } else if (itemCombo.getValue().equals("iHupelin")){    /*wehn iHupelin is chosen*/
                distance = checkDistance(coordinates);
                if(endAutomat.getItems().indexOf(endAutomat.getValue()) == -1 /*somtehing's wrong*/
                    || startautomatCombo.getItems().indexOf(startautomatCombo.getValue()) == -1){
                    tooFarLabel.setText("Valitse kaupungit ja automaatit.");    /*tells the user waht to do*/
                } else {    /*things go smooth*/
                    AppleItem AI = new AppleItem();
                    name = "iHupelin";
                    PClass3 packet2 = new PClass3(AI.getName(), AI.getHeight(), AI.getDepth(),  /*new packet*/
                            AI.getWidth(), AI.getWeight(), AI.getBreak(), toC, fromC, 3, fromName, toName, distance);
                    lc.packetcreated(name, startCityCombo.getValue(), startautomatCombo.getValue(), endCity.getValue(), endAutomat.getValue(), distance);   /*adds to log*/
                    storage.addpackage(packet2);    /*adds packet to storage*/
                }
            }
        } 
        clearAction(); /*clears all textfields and combos so that new packet can easily be created yayyyy*/
    }
    
    
    
    @FXML
    private void startAutomatComboAction(ActionEvent event) {
    }
    
    private double checkDistance(ArrayList coords) {    /*finds out the distance*/
        double length = (double) web2.getEngine().executeScript("document.createPath("+coords+", 'red' , 1 )"); /*creates path to the map in webview*/
        return length;      /*and returns length*/
    }
    
    private void hideErrorMessages() { /*hides all error labels*/
        nameLabel.setText("");
        sizeLabel.setText("");
        weightLabel.setText("");
        startLabel.setText("");
        endLabel.setText("");
        allLabel.setText("");
        tooBigError.setText("");
        tooBigError2.setText("");
        tooFarLabel.setText("");
    }
    
    public void tooBigError(int x, int y, int z,double w, int c) { /*tells user whether the item is too big or too heavy*/
        if (c == 1) { /*first class item*/
            if (x > 60 || y > 40 || z > 60){
                tooBigError.setText("Max mitat 60*40*60!");
            }if (w > 30) {
                tooBigError2.setText("Max paino 30 kg!");
            }
        }else if (c == 2) { /*second class item*/
            if (x > 30 || y > 20 || z > 30){
                tooBigError.setText("Max mitat 30*20*30!");
            }if (w > 12) {
                tooBigError2.setText("Max paino 12 kg!");
            }
        }else{ /*third class item*/
            if (x > 80 || y > 60 || z > 80){
                tooBigError.setText("Max mitat 80*60*80!");
            }if (w > 50){
                tooBigError2.setText("Max paino 50 kg!");
            }
        }
    }
    
    private void tooFarError(double x){ /*when smartpost automats are too far away from each other*/
        double d = 150;
        double temp = x - 150;
        tooFarLabel.setText("Kohteiden välinen matka on " + round(temp) + " km sallittua pidempi."); /*informs the user*/
    }
    
    private void errorMessages() { /*shows the error labels when create packet button is presses*/
        if (nameField.getText().equals("") && sizeField.getText().equals("") && sizeField2.getText().equals("") &&
                    sizeField3.getText().equals("") && weightField.getText().equals("") &&
                    startautomatCombo.getItems().indexOf(startautomatCombo.getValue()) == -1 && 
                    endAutomat.getItems().indexOf(endAutomat.getValue()) == -1 &&
                    itemCombo.getItems().indexOf(itemCombo.getValue()) == -1) {  /*if all textfields and combos are empty*/
                allLabel.setText("Valitse valmis esine tai luo uusi!");
            } else {
            if (nameField.getText().equals("")) {   /*when namefield is empty*/
                nameLabel.setText("Nimi puuttuu!");
            } if (sizeField.getText().equals("") || sizeField2.getText().equals("") || sizeField3.getText().equals("")) { /*when size isn't displayed correctly */
                sizeLabel.setText("Anna koko muodossa cm*cm*cm!");
            } if (weightField.getText().equals("")) { /*weight field is empty*/
                weightLabel.setText("Massa puuttuu!");
            } if (startautomatCombo.getItems().indexOf(startautomatCombo.getValue()) == -1) { /*nothing is chosen in city and automat combos*/
                startLabel.setText("Valitse lähtökaupunki ja automaatti!");
            } if (endAutomat.getItems().indexOf(endAutomat.getValue()) == -1) {
                endLabel.setText("Valitse kohdekaupunki ja automaatti!");
            } 
        }
    }
            
    private void itemComboSetting() { /*creates items and adds their names to itemCombo*/
        DogeItem DI = new DogeItem();
        itemCombo.getItems().add(DI.getName());
        CoffeeItem CI = new CoffeeItem();
        itemCombo.getItems().add(CI.getName());
        BookItem BI = new BookItem();
        itemCombo.getItems().add(BI.getName());
        AppleItem AI = new AppleItem();
        itemCombo.getItems().add(AI.getName());
    }
    
    private void combosetting() throws IOException{ /*adds cities to startcity and endcity combos*/
        URL url1 = new URL("http://smartpost.ee/fi_apt.xml");
        BufferedReader br = new BufferedReader(new InputStreamReader(url1.openStream()));
        String content = "";
        String line;
        while ((line = br.readLine()) != null) {
            content += line + "\n";
        }
        parsinta p = new parsinta(content);
        automatlist = p.getArray();
        for (pakettiautomaatti pa : automatlist){
            if(endCity.getItems().contains(pa.getCity())){
                
            }else {
                endCity.getItems().add(pa.getCity());
            }
            
            if(startCityCombo.getItems().contains(pa.getCity())){
                
            }else {
                startCityCombo.getItems().add(pa.getCity());
            }
        }        
    }
    
    public void bringList (ArrayList Alist){
        automatlist = Alist;
    }
    
    private void clearAction() { /*sets all combos to null and clear textfields*/
        endAutomat.valueProperty().set(null);
        endCity.valueProperty().set(null);
        startautomatCombo.valueProperty().set(null);
        startCityCombo.valueProperty().set(null);
        itemCombo.valueProperty().set(null);
        nameField.clear();
        sizeField.clear();
        sizeField2.clear();
        sizeField3.clear();
        weightField.clear();
        if (breakingStuffBox.isSelected()) { /*unchecks the breakability box when necessary*/
            breakingStuffBox.fire();
        }
        firstClass.setSelected(true);
    }
}
