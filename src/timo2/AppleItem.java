package timo2;

public class AppleItem {
    private String Name;
    private int Height;
    private int Depth;
    private int Width;
    private double Weight;
    private boolean Breakability;
    private int PClass;
    
    public AppleItem(){
        Name = "iHupelin";
        Height = 12;
        Depth = 1;
        Width = 6;
        Weight = 0.4;
        Breakability = true; /*hajoaa aina mwahahahahahhaaaaaa*/
        PClass = 3;
    }
    
    public String getName(){
        return Name;
    }
    public int getHeight(){
        return Height;
    }
    public int getDepth(){
        return Depth;
    }
    public int getWidth(){
        return Width;
    }
    public double getWeight(){
        return Weight;
    }
    public boolean getBreak(){
        return Breakability;
    }
    public int getPClass(){
        return PClass;
    }
}
