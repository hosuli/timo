package timo1;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Math.round;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class LokiController implements Initializable {
    
    private static LokiController instance = null;
    @FXML
    private ImageView lokiView;
    private String name;
    private String startcity;
    private String startautomat;
    private String endautomat;
    private String endcity;
    private double length;
    private String text;
    private int doesBreak;
    @FXML
    private TextArea logArea;
    private int storageCounter = 0; /*packets in the storage*/
    private int sentCounter = 0;    /*packets sent*/
    @FXML
    private Label storageLabel;
    @FXML
    private Label sentLabel;

    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {    /*executes when program closes*/
            public void run() {
                try {
                    saveReceipt();  /*saves the receipt*/
                } catch (IOException ex) {
                    Logger.getLogger(LokiController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, "Shutdown-thread"));
        instance = this;
        Image image = new Image("loki.jpg");  /*sets up picture og the mighty Loki*/
        storageLabel.setText("Varasto on tyhjä.");
        sentLabel.setText("Yhtään pakettia ei ole lähetetty.");
        lokiView.setImage(image);
        logArea.setText("LOKI\n\n");
        logArea.appendText("***********************\n\n");

    }
    
    public static LokiController getInstance(){
        return instance;
    }
    
    public void packetcreated(String n, String sc, String sa, String ec, String ea, double l) { /*prints information abt created packets to the log*/
        name = n;
        startcity = sc;
        startautomat = sa;
        endcity = ec;
        endautomat = ea;
        length = l;
        
        logArea.appendText("Varastoon on saapunut paketti\n\nNimi: "+name+"\nFrom: "+startcity+" "
                + " "+startautomat+"\nTo: "+endcity+ " "+endautomat+"\nMatkan pituus: "+round(length) + " km\n\n***********************\n\n");
        storageCounter= storageCounter + 1;     /*packets in storage +1*/
        storageLabel.setText("Varastossa paketteja "+storageCounter+" kpl.");
    }
    
    public void packetSent(String n, String sc, String sa, String ec, String ea, double l,int db){  /*prints information abt sent packets to the log*/
        name = n;
        startcity = sc;
        startautomat = sa;
        endcity = ec;
        endautomat = ea;
        length = l;
        doesBreak = db;
        logArea.appendText("Varastosta on lähetetty paketti\n\nNimi: "+name+"\nFrom: "+startcity+" "
                + " "+startautomat+"\nTo: "+endcity+ " "+endautomat+"\nMatkan pituus: "+round(length)+" km");
        if (doesBreak == 1) {
            logArea.appendText("\nIkävä kyllä paketti hajosi kuljetuksessa.\n\n***********************\n\n");
        } else {
            logArea.appendText("\n\n***********************\n\n");
        }
        storageCounter = storageCounter - 1;    /*packets in storage -1*/
        storageLabel.setText("Varastossa paketteja "+storageCounter+" kpl.");
        sentCounter = sentCounter + 1;  /*packets sent +1*/
        sentLabel.setText("Paketteja on lähetetty "+sentCounter+" kpl.");
    }
    
    public void saveReceipt() throws IOException{   /*saves all information in the log to a receipt.txt*/
        BufferedWriter bw = new BufferedWriter(new FileWriter("receipt.txt"));  /*new bufferedwriter and filewriter, creates receipt.txt*/
        bw.write("*********Kuitti*******");
        bw.newLine();
        bw.newLine();
        bw.write(storageLabel.getText());
        bw.newLine();
        bw.write(sentLabel.getText());
        bw.newLine();
        bw.newLine();
        bw.newLine();
        text = logArea.getText();
        String line[] = text.split("\\n");
        for (String lines: line) {  /*without this for loop everything sucks*/
            bw.write(lines);
            bw.newLine();
        }
        bw.close();
    }
    
}
